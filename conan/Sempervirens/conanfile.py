from conans import ConanFile, CMake, tools


class SempervirensConan(ConanFile):
    name = "sempervirens"
    version = "1.0"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Sempervirens here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake_paths"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        self.run("git clone https://gitlab.com/nico-dog/sempervirens.git")
        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly
        #tools.replace_in_file("hello/CMakeLists.txt", "PROJECT(HelloWorld)",
        #                      '''PROJECT(HelloWorld)
        #                      include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
        #                      conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="sempervirens")
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        #cmake = CMake(self)
        #cmake.configure(source_folder="sempervirens")
        #cmake.install()
        self.copy("*.hpp", dst="include/Sempervirens", src="sempervirens/include")
        self.copy("*.hpp", dst="include/Sempervirens/Application", src="sempervirens/src/Application")
        self.copy("*.hpp", dst="include/Sempervirens/Controller", src="sempervirens/src/Controller")
        self.copy("*.inl", dst="include/Sempervirens/Controller", src="sempervirens/src/Controller")
        self.copy("*.hpp", dst="include/Sempervirens/EventSystem", src="sempervirens/src/EventSystem")
        self.copy("*.inl", dst="include/Sempervirens/EventSystem", src="sempervirens/src/EventSystem")
        self.copy("*.hpp", dst="include/Sempervirens/Keyboard", src="sempervirens/src/Keyboard")
        self.copy("*.hpp", dst="include/Sempervirens/Keyboard/Platform/Linux", src="sempervirens/src/Keyboard/Platform/Linux")
        self.copy("*.hpp", dst="include/Sempervirens/Logging", src="sempervirens/src/Logging")
        self.copy("*.hpp", dst="include/Sempervirens/Macros", src="sempervirens/src/Macros")
        self.copy("*.hpp", dst="include/Sempervirens/MemoryAlloc", src="sempervirens/src/MemoryAlloc")
        self.copy("*.inl", dst="include/Sempervirens/MemoryAlloc", src="sempervirens/src/MemoryAlloc")
        self.copy("*.hpp", dst="include/Sempervirens/Mouse", src="sempervirens/src/Mouse")
        self.copy("*.hpp", dst="include/Sempervirens/Mouse/Platform/Linux", src="sempervirens/src/Mouse/Platform/Linux")
        self.copy("*.hpp", dst="include/Sempervirens/Timer", src="sempervirens/src/Timer")
        self.copy("*.hpp", dst="include/Sempervirens/Timer/Platform/Linux", src="sempervirens/src/Timer/Platform/Linux")
        self.copy("*.hpp", dst="include/Sempervirens/UnitTesting", src="sempervirens/src/UnitTesting")
        self.copy("*.inl", dst="include/Sempervirens/UnitTesting", src="sempervirens/src/UnitTesting")
        self.copy("*.hpp", dst="include/Sempervirens/Window", src="sempervirens/src/Window")
        self.copy("*.hpp", dst="include/Sempervirens/Window/Platform/Linux", src="sempervirens/src/Window/Platform/Linux")
        self.copy("*.hpp", dst="include/Sempervirens/WSI", src="sempervirens/src/WSI")

        self.copy("SempervirensConfigVersion.cmake", dst="lib/cmake/Sempervirens", src=".")
        self.copy("SempervirensConfig.cmake", dst="lib/cmake/Sempervirens", src=".")
        self.copy("SempervirensTargets.cmake", dst="lib/cmake/Sempervirens", src="cmake")
        
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["Sempervirens"]

